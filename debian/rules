#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk
include /usr/share/rustc/architecture.mk

export BUILD_MODE=release-deb

export CARGO=/usr/share/cargo/bin/cargo

export CFLAGS CXXFLAGS CPPFLAGS LDFLAGS
export DEB_HOST_RUST_TYPE DEB_HOST_GNU_TYPE
export CARGO_HOME=$(CURDIR)/debian/cargo_home

export DEB_CARGO_CRATE=proxmox-offline-mirror_$(DEB_VERSION_UPSTREAM)
export DEB_CARGO_PACKAGE=proxmox-offline-mirror

# for docs build
export DEB_VERSION DEB_VERSION_UPSTREAM

%:
	dh $@ --with=bash-completion

override_dh_auto_configure:
	@perl -ne 'if (/^version\s*=\s*"(\d+(?:\.\d+)+)"/) { my $$v_cargo = $$1; my $$v_deb = "$(DEB_VERSION_UPSTREAM)"; \
	    die "ERROR: d/changelog <-> Cargo.toml version mismatch: $$v_cargo != $$v_deb\n" if $$v_cargo ne $$v_deb; exit(0); }' Cargo.toml
	$(CARGO) prepare-debian $(CURDIR)/debian/cargo_registry --link-from-system
	# `cargo build` and `cargo install` have different config precedence, symlink
	# the wrapper config into a place where `build` picks it up as well..
	# https://doc.rust-lang.org/cargo/commands/cargo-install.html#configuration-discovery
	mkdir -p $(CURDIR)/.cargo
	ln -s $(CARGO_HOME)/config.toml $(CURDIR)/.cargo/config.toml
	dh_auto_configure

override_dh_auto_test:
	# skip for now to avoid additional debug builds - no tests anyway
	# dh_auto_test -- test --all

execute_after_dh_auto_install:
	/usr/share/cargo/bin/dh-cargo-built-using $(DEB_CARGO_PACKAGE)

override_dh_missing:
	dh_missing --fail-missing

override_dh_compress:
	dh_compress -X.pdf

override_dh_strip:
	dh_strip
	for exe in $$(find \
	    debian/proxmox-offline-mirror/usr \
	    debian/proxmox-offline-mirror-helper/usr \
	    -executable -type f); \
	do \
	  debian/scripts/elf-strip-unused-dependencies.sh "$$exe" || true; \
	done
