rust-proxmox-offline-mirror (0.6.7) bookworm; urgency=medium

  * pool: fix check for empty directory when unlinking files

  * pool gc: remove empty left-over directories

  * add ability to verify with keyrings, not just single certificates

  * docs: fix command arguments for syncing a medium

 -- Proxmox Support Team <support@proxmox.com>  Fri, 06 Sep 2024 17:50:35 +0200

rust-proxmox-offline-mirror (0.6.6) bookworm; urgency=medium

  * fix #5249: apt: allow parsing Packages without Priority field

 -- Proxmox Support Team <support@proxmox.com>  Fri, 19 Apr 2024 09:01:51 +0200

rust-proxmox-offline-mirror (0.6.5) bookworm; urgency=medium

  * docs: fix version and release numbers

 -- Proxmox Support Team <support@proxmox.com>  Wed, 21 Feb 2024 12:15:50 +0100

rust-proxmox-offline-mirror (0.6.4) bookworm; urgency=medium

  * helper: improve handling of multiple keys when activating them

  * docs: add an auto dark mode to the docs

  * weak crypto: actually honor RSA config option

  * docs: offline-media: Use correct config subcommand

  * minor code cleanup

 -- Proxmox Support Team <support@proxmox.com>  Tue, 09 Jan 2024 09:33:28 +0100

rust-proxmox-offline-mirror (0.6.3) bookworm; urgency=medium

  * add non-free-firmware to bookworm default components

  * docs: fix bullseye references

  * docs: fix copied env variable documentation

  * add support for ceph reef

  * add missing subscription setting for ceph enterprise repos

 -- Proxmox Support Team <support@proxmox.com>  Sat, 25 Nov 2023 17:22:31 +0100

rust-proxmox-offline-mirror (0.6.2) bookworm; urgency=medium

  * d/rules: patch out wrongly linked libraries from ELFs

  * release file: extend component fixup for Debian security repo also to
    bookworm suite

 -- Proxmox Support Team <support@proxmox.com>  Thu, 29 Jun 2023 15:04:12 +0200

rust-proxmox-offline-mirror (0.6.1) bookworm; urgency=medium

  * add support for bookworm enterprise ceph repo

  * add bookworm to the list of releases for easier set up

 -- Proxmox Support Team <support@proxmox.com>  Fri, 16 Jun 2023 09:29:40 +0200

rust-proxmox-offline-mirror (0.6.0) bookworm; urgency=medium

  * re-build for Debian Bookworm based release

  * switch to native versioning

 -- Proxmox Support Team <support@proxmox.com>  Tue, 30 May 2023 13:05:47 +0200

rust-proxmox-offline-mirror (0.5.1-1) bullseye; urgency=medium

  * fix #4445: add proxy support by honoring ALL_PROXY environment variable
    for mirroring and subscription management

  * docs: document `ALL_PROXY` environment variable

 -- Proxmox Support Team <support@proxmox.com>  Tue, 07 Feb 2023 15:35:17 +0100

rust-proxmox-offline-mirror (0.5.0-1) stable; urgency=medium

  * fix #4259: mirror: add 'ignore-errors' option to make fetching errors from
    (technically) broken repositories non-fatal.

  * mirror: collect and summarize warnings to ensure that they ain't missed in
    the rather verbose output

  * pool: add a 'diff' command for snapshots and for mediums, to ease
    comparing the actual changes

  * mirror snapshot list: make the <id> parameter optional so that one can
    quickly list all snapshots from all mirrors.

  * mirror: add option to configure filtering one or more packages via GLOBs
    and also specific Debian packaging sections like 'games', 'kernel' or
    'debug' to reduce the pool size

  * mirror: implement source packages mirroring

  * fix #4264: only require either Release or InRelease as even though that
    InRelease is strictly speaking required, some repos (especially older
    ones) may not have it.

  * guided mirror setup: propose to filter out the 'games' and 'debug'
    sections by default, which, for example, reduces a mirror of PVE + Debian
    bullseye by about 27% (105GB->77GB).

  * docs: mention the new section and package filters

  * helper: add status command for showing information that is stored on the
    medium itself

  * guided setup: add Quincy as supported release for bullseye

 -- Proxmox Support Team <support@proxmox.com>  Sat, 22 Oct 2022 17:01:07 +0200

rust-proxmox-offline-mirror (0.4.0-1) stable; urgency=medium

  * docs: add minimal sample nginx config

  * docs: offline keys: add info about minimum product version that got
    activation support

  * docs: expand/structure "using medium" section

  * mirror: use xz multi decoder to be compatible with Ubuntu and similar
    usage of xz

  * mirror: skip failed, non Packages references for better compat with
    non-Debian/Proxmox repos

  * mirror: handle indices which are only available compressed and support
    acquiring them by hash

  * mirror: add --dry-run parameter

  * cli: add `mirror snapshot create-all` command, which creates a new
    snapshot for each configured mirror. This should be suitable for usage in
    a cron job or systemd timer-triggered unit

 -- Proxmox Support Team <support@proxmox.com>  Fri, 16 Sep 2022 14:25:59 +0200

rust-proxmox-offline-mirror (0.3.0-1) stable; urgency=medium

  * pool: allow pool re-use (breaking change!)

  * pool: improve sync progress reporting

  * docs: lots of improvements

  * rename proxmox-apt-repo to proxmox-offline-mirror-helper

  * move proxmox-offline-mirror-helper into own package

 -- Proxmox Support Team <support@proxmox.com>  Fri, 9 Sep 2022 13:50:43 +0200

rust-proxmox-offline-mirror (0.2.0-1) stable; urgency=medium

  * split out docs package

  * update proxmox dependencies to reduces indirect, transitive dependencies

  * keys: improve output on add/refresh

  * wizard: ask for new medium ID up front

  * docs pdf: avoid extra blank page on section/chapter boundary

  * docs: restructure and split in different chapter files

  * docs: add installation chapter

  * medium: implement `remove .. --remove-data`

  * wizard: optionally auto-add Debian mirrors

  * wizard: query base path for location

  * docs: ship config manpage as proxmox-offline-mirror.cfg.5

 -- Proxmox Support Team <support@proxmox.com>  Thu, 08 Sep 2022 13:46:05 +0200

rust-proxmox-offline-mirror (0.1.0-1) stable; urgency=medium

  * initial release

 -- Proxmox Support Team <support@proxmox.com>  Tue, 6 Sep 2022 09:54:10 +0200
