This tool allows registering and updating offline subscription keys, and
configuring and managing offline APT repository mirrors and media.