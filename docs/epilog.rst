.. Epilog (included at top of each file)

   We use this file to define external links and common replacement
   patterns.

.. |WEBSITE| replace:: https://www.proxmox.com
.. |DOWNLOADS| replace:: https://www.proxmox.com/downloads

.. _Proxmox: https://www.proxmox.com
.. _Proxmox Community Forum: https://forum.proxmox.com
.. _Proxmox Virtual Environment: https://www.proxmox.com/proxmox-ve
.. _Proxmox Backup: https://pbs.proxmox.com/wiki/index.php/Main_Page
.. _PVE Development List: https://lists.proxmox.com/cgi-bin/mailman/listinfo/pve-devel
.. _reStructuredText: https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html
.. _Rust: https://www.rust-lang.org/
.. _SHA-256: https://en.wikipedia.org/wiki/SHA-2

.. _AGPL3: https://www.gnu.org/licenses/agpl-3.0.en.html
.. _Debian: https://www.debian.org
