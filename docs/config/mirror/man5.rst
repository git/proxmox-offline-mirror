==========================
proxmox-offline-mirror.cfg
==========================

Description
===========

The file ``/etc/proxmox-offline-mirror.cfg`` is a configuration file for Proxmox Offline Mirror. It
contains definitions for mirrors, media and subscription keys.

Note that you can use the ``--config <file>`` switch on most commands or the ``PROXMOX_OFFLINE_MIRROR_CONFIG`` environment variable to override the default config location.

Options
=======

.. include:: config.rst

.. include:: ../../pom-copyright.rst
