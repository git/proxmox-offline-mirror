Command Syntax
==============

``proxmox-offline-mirror``
--------------------------

For supported environment variables please refer to
:ref:`env_vars` .

.. include:: proxmox-offline-mirror/synopsis.rst


``proxmox-offline-mirror-helper``
---------------------------------

.. include:: proxmox-offline-mirror-helper/synopsis.rst
